const express = require('express');
const Redis = require('ioredis');

const app = express();
const port = 3000;

const redis = new Redis({
  host: 'redis',
  port: 6379
});

app.use(express.json());

app.post('/message', async (req, res) => {
  const { message } = req.body;
  if (!message) {
    return res.status(400).send('Message is required');
  }
  await redis.lpush('messages', message);
  res.status(201).send('Message added');
});

app.get('/messages', async (req, res) => {
  const messages = await redis.lrange('messages', 0, -1);
  res.status(200).json(messages);
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
